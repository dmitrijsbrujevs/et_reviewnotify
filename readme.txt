== Extension permanent link
Permanent link: http://shop.etwebsolutions.com/eng/et-reviewnotify.html
Support link: http://support.etwebsolutions.com/projects/et-reviewnotify/roadmap

== Short Description
Extension notifies store owner about new reviews
and complicates automatic review posting for spam bots.

== Version Compatibility
Magento CE:
1.3.х (tested in 1.3.2.4)
1.4.x (tested in 1.4.1.1)
1.5.x (tested in 1.5.0.1 and 1.5.1.0)
1.6.x (tested in 1.6.1.0)
1.7.x (tested in 1.7.0.2)
1.8.x (tested in 1.8.0.0)
1.9.x (tested in 1.9.1.0)

== Installation
* Disable compilation if it is enabled (System -> Tools -> Compilation)
* Disable cache if it is enabled (System -> Cache Management)
* Download the extension or install the extension from Magento Connect
* If you have downloaded it, copy all files from the "install" folder to the Magento root folder - where your index.php is
* Log out from the admin panel
* Log in to the admin panel with your login and password
* Set extension's parameters (System -> Configuration -> Catalog -> Product Reviews)
* Run the compilation process and enable cache if needed